import calculator as c
import Dodawanie_i_odejmowanie as d

if __name__=='__main__':
    print('Kalkulator')
    print('1. Potęgowanie')
    print("2. Dodawanie")
    print("3. Odejmowanie")
    print('0. Wyjśćie')
    while True:
        try:
            choice=int(input('Operacja:'))
            if choice==0:
                break
            a=int(input('Pierwsza liczba:'))
            b=int(input('Druga liczba:'))
            # Tu implenentujcie swoje funkcje jako np. if choice==1 (pamiętajcie o dodaniu ich do menu wyżej
            if choice==1:
                print(f'Wynik: {c.potega(a, b)}')
            elif choice == 2:
                print(d.dodaj(a, b))
            elif choice == 3:
                print(d.odejmij(a, b))
        except:
            print('Wprowadź liczbę całkowitą!')
